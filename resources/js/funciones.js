
var validarEmail = (email)=>{
    let regex = /^[_a-z0-9-]+(.[_a-z0-9-]+)*@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,4})$/i;

    if(regex.test(email)) {
        return true;
    } else {
        return false
    }
}

// Función que captura un parámetro enviado por la URL
var getParameterByName = (name) => {
    // el método replace() busca dentro de una cadena caracteres especificados y los reemplaza por otros
    // nos retorna una nueva cadena con los reemplazos realizados
    name      = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)");
    results   = regex.exec(location.search);

    return results === null ? null : decodeURIComponent(results[1].replace(/\+/g, " ")); 
}

var printThisDiv = (id) => {
    var printContents    = document.getElementById(id).innerHTML;
    var originalContents = document.body.innerHTML;
    document.body.innerHTML = printContents;
    window.print();
    document.body.innerHTML = originalContents;
}
