class Usuarios extends Uploadpicture {
    constructor () {
        super();
        this.Funcion    = 0;  // 0-New  1-Update
        this.IdUsuario  = 0;
        this.Imagen     = null;
    }

    loginUser() {
        // if(email == "") {
        //     document.getElementById("email").focus();
        //     M.toast({ html: 'Ingrese el email', classes: 'rounded' });
        // } else {
        //     if(password == "") {
        //         document.getElementById("password").focus();
        //         M.toast({ html: 'Ingrese el password', classes: 'rounded' });
        //     } else {

        if(validarEmail(document.getElementById("email").value)) {
            // if(8 <= password.length) {

                // Enviando datos por Post al servidor ...
            $.post(
                "index/userLogin",
                    // { email, password },
                $('.login').serialize(),
                (response)=>{
                    console.log(response); // Procesando la respuesta del servidor ...

                    if (response == 1) {
                        document.getElementById("password").focus();
                        document.getElementById("indexMessage").innerHTML = "Ingrese el password";
                    } else {
                        if (response == 2) {
                            document.getElementById("password").focus();
                            document.getElementById("indexMessage").innerHTML = "Introduzca al menos 8 caracteres";
                        } else {
                            try {
                                var item = JSON.parse(response); 
        
                                if (0 < item.idUsuario) {
                                    localStorage.setItem("user", response);
                                    window.location.href = URL + "principal/principal";    
                                } else {
                                    document.getElementById('indexMessage').innerHTML = "Email o contraseña incorrectos";
                                }
        
                            } catch (error) {
                                document.getElementById('indexMessage').innerHTML = response;
                            }
                        }
                    }
                }
            );

            // } else {
            //     document.getElementById("password").focus();
            //     M.toast({ html: 'Ingrese al menos 8 caracteres', classes: 'rounded' });
            // }
        } else {
            document.getElementById("email").focus();
            document.getElementById('indexMessage').innerHTML = "Ingrese un email válido";
            // M.toast({ html: 'Ingrese un email válido', classes: 'rounded' });
        }   
            //}    
        //}
    }

    userData(URLactual) {
        if(PATHNAME == URLactual) {
            localStorage.removeItem("user");
            document.getElementById('menuNavBar1').style.display = 'none';
            document.getElementById('menuNavBar2').style.display = 'none';
        } else {
            if(null != localStorage.getItem("user")) {
                let user = JSON.parse(localStorage.getItem("user"));
                
                if(0 < user.idUsuario) {
                    document.getElementById('menuNavBar1').style.display = 'block';
                    document.getElementById('menuNavBar2').style.display = 'block';
                    // $("#name1").append(user.pnombre + ' ' + user.apellidos);
                    document.getElementById('name1').innerHTML = user.pnombre + ' ' + user.apellidos;
                    // $("#role1").append(user.roles);
                    document.getElementById('role1').innerHTML = user.roles;
                    // $("#name2").append(user.pnombre + ' ' + user.apellidos);
                    document.getElementById('name2').innerHTML = user.pnombre + ' ' + user.apellidos;
                    // $("#role2").append(user.roles);
                    document.getElementById('role2').innerHTML = user.roles;

                    document.getElementById('fotoUser1').innerHTML = ['<img class="responsive-img valign profile-image" src="',URL+FOTOS+'usuarios/'+user.imagen,'" title="',escape(user.imagen),'"/>'].join('');
                    document.getElementById('fotoUser2').innerHTML = ['<img class="responsive-img valign profile-image" src="',URL+FOTOS+'usuarios/'+user.imagen,'" title="',escape(user.imagen),'"/>'].join('');

                }
            }
        }
    }

    getRoles(role, funcion) {
        let count = 1;
        $.post(
            URL+"Usuarios/getRoles",
            {},
            (response)=>{
                try {
                    let item = JSON.parse(response);
                    console.log(item);
                    document.getElementById('roles').options[0] = new Option("Seleccione un role",0);

                    if (0 < item.results.length) {
                        for (let i = 0; i < item.results.length; i++) {

                            switch (funcion) {
                                case 1:
                                    document.getElementById('roles').options[count] = new Option(item.results[i].role,item.results[i].idRole); 
                                    count++;
                                    $('select').formSelect(); // Re-inicializar los select de la página
                                    break;
                                case 2:
                                    document.getElementById('roles').options[count] = new Option(item.results[i].role,item.results[i].idRole);
                                    if (item.results[i].role == role) {
                                        i++;
                                        document.getElementById("roles").selectedIndex = i;
                                        i--;
                                    }
                                    count++;
                                    $('select').formSelect(); // Re-inicializar los select de la página
                                    break;
                                default:
                                    break;
                            }
                        }
                    }

                } catch (error) {
                    
                }                
            }
        );
    }

    // registerUser(pnombre, snombre, apellido, nid, telefono, email, password, user, roles) {
    registerUser() {

        let valor = false;
        // if(validarEmail(email)) {
        if(validarEmail(document.getElementById("email").value)) {    
        
            // if(8 <= password.length) {
            
            var data = new FormData();
            $.each(
                $('input[type=file]')[0].files, (i, file) => {
                    data.append('file', file);
                }                        
            );

            var url = this.Funcion == 0 ? "Usuarios/registerUser" : "Usuarios/editUser";

            let roles = document.getElementById("roles");
            let role  = roles.options[roles.selectedIndex].text;
            
            // console.log("id usuario: " + this.IdUsuario);    

            data.append('idUsuario',this.IdUsuario)
            data.append('pnombre', document.getElementById("pnombre").value);    
            data.append('snombre', document.getElementById("snombre").value);    
            data.append('apellido', document.getElementById("apellido").value);    
            data.append('nid', document.getElementById("nid").value);    
            data.append('telefono', document.getElementById("telefono").value);    
            data.append('email', document.getElementById("email").value);    
            data.append('password', document.getElementById("password").value);    
            data.append('usuario', document.getElementById("usuario").value);    
            data.append('roles', role); 
            data.append('imagen', this.Imagen);    
            
            // console.log(data.get('pnombre'));    
            
            $.ajax({
                url: URL + url,
                data: data,
                cache: false,
                contentType: false,
                processData: false,
                type: 'POST',
                success: (response) => {
                    if (response == 0) {
                        restablecerUser();
                    } else {
                        valor = true;
                        document.getElementById('registerMessage').innerHTML = response;
                    }
                }
            });

            // } else {
            //     document.getElementById("password").focus();
            //     document.getElementById('registerMessage').innerHTML = 'Ingrese al menos 8 caracteres';
            // }

        } else {
            document.getElementById("email").focus();
            document.getElementById('registerMessage').innerHTML = 'Ingrese un email válido';
            valor = true;
        }

        return valor;

    }

    getUsers(valor, page) {
        // alert(valor);
        var valor = valor != null ? valor : "";

        $.post(
            URL + 'Usuarios/getUsers',
            { filter : valor, page: page },
            (response) => {
                try {
                    let item = JSON.parse(response);
                    $("#resultUser").html(item.dataFilter);
                    $("#paginador").html(item.paginador);
                    console.log(item);
                } catch (error) {
                    $("#paginador").html(response);
                }
                //console.log(response);
            }
        );
    }

    // Editar usuarios

    editUser(data) {
        this.Funcion   = 1;
        this.IdUsuario = data.idUsuario;
        this.Imagen    = data.imagen;

        document.getElementById("fotos").innerHTML = ['<img class="responsive-img" src="',PATHNAME + "resources/images/fotos/usuarios/" + data.imagen,'" title="',escape(data.imagen),'"/>'].join('');

        document.getElementById("pnombre").value     = data.pnombre;
        document.getElementById("snombre").value     = data.snombre;
        document.getElementById("apellido").value    = data.apellidos;
        document.getElementById("nid").value         = data.nid;
        document.getElementById("telefono").value    = data.telefono;
        document.getElementById("email").value       = data.email;
        document.getElementById("usuario").value     = data.usuario;

        document.getElementById("password").value    = '**********';
        document.getElementById("password").disabled = true;
        
        this.getRoles(data.roles, 2);
    }

    deleteUser(data) {
        $.post(
            URL + 'Usuarios/deleteUsers',
            { idUsuario: data.idUsuario, email: data.email },
            (response) => {
                if(response==0) {
                    this.restablecerUser();
                } else {
                    document.getElementById("dateUserMessage").innerHTML = response;
                }
                console.log(response);
            }
        );
    }

    restablecerUser() {

        this.Funcion    = 0;
        this.IdUsuario  = 0;
        this.Imagen     = null;

        document.getElementById('fotos').innerHTML = ['<img class="responsive-img" src="',PATHNAME + "resources/images/fotos/usuarios/default.png",'" title="',,'"/>'].join('');
        this.getRoles(null, 1);
        // $('#modal1').hide();
        var instance1 = M.Modal.getInstance($('#modal1'));
        instance1.close();
        var instance2 = M.Modal.getInstance($('#modal2'));
        instance2.close();

        document.getElementById('pnombre').value  = "";
        document.getElementById('snombre').value  = "";
        document.getElementById('apellido').value = "";      
        document.getElementById('nid').value      = "";
        document.getElementById('telefono').value = "";
        document.getElementById('email').value    = "";
        document.getElementById('password').value = "";
        document.getElementById('usuario').value  = "";

        // this.getUsers(null,1);
        window.location.href = URL + "Usuarios/usuarios";
    }

    sessionClose() {
        localStorage.removeItem("user");
        document.getElementById('menuNavBar1').style.display = 'none';
        document.getElementById('menuNavBar2').style.display = 'none';
    }
   
}
