class Uploadpicture {
    constructor() {

    } 

    // Cargamos un archivo de tipo imagen desde el pc del cliente
    // @evt objeto de tipo lista de archivos

    archivo(evt, id) {
        let files = evt.target.files;  // <-- File list object (files queda convertido en un array)
        let f     = files[0];

        if (f.type.match('image.*')) {
            let reader = new FileReader();
            reader.onload = ((theFile) => {
                return(e) => {
                    // insertamos la imagen
                    document.getElementById(id).innerHTML = ['<img class="responsive-img" src="',e.target.result,'" title="',escape(theFile.name),'"/>'].join('');
                }
            })(f);
            reader.readAsDataURL(f);
        }
    }

}