/** Código de USUARIOS */
var data_User = null;
var usuarios  = new Usuarios();

var loginUser = ()=>{
    // var email    = document.getElementById('email').value;
    // var password = document.getElementById('password').value;
    // usuarios.loginUser(email, password);
    usuarios.loginUser();
} 

$(function(){
    // Pagina de login (Para que el enter envíe el formulario)
    $("#email").keyup(function(event) {
        if (event.which === 13) {
            $("#btnLogin").click();
        }
    });
    
    $('#password').keyup(function (event) {
        if (event.which === 13) {
            $("#btnLogin").click();
        }
    });
});

var sessionClose = () => {
    usuarios.sessionClose();
}

var restablecerUser = () => {
    usuarios.restablecerUser();
}

var archivo = (evt) => {
    usuarios.archivo(evt, "fotos");
}

var getRoles = () => {
    usuarios.getRoles(null, 1);
}

// var registerUser = () => {
//     let pnombre  = document.getElementById('pnombre').value;
//     let snombre  = document.getElementById('snombre').value;
//     let apellido = document.getElementById('apellido').value;      
//     let nid      = document.getElementById('nid').value;
//     let telefono = document.getElementById('telefono').value;
//     let email    = document.getElementById('email').value;
//     let password = document.getElementById('password').value;
//     let roles    = document.getElementById('roles');
//     let role     = roles.options[roles.selectedIndex].text;

//     usuarios.registerUser(pnombre, snombre, apellido, nid, telefono, email, password, role);
// }

$(function(){
    $('#registerUser').click(function(){
        // let pnombre  = document.getElementById('pnombre').value;
        // let snombre  = document.getElementById('snombre').value;
        // let apellido = document.getElementById('apellido').value;      
        // let nid      = document.getElementById('nid').value;
        // let telefono = document.getElementById('telefono').value;
        // let email    = document.getElementById('email').value;
        // let password = document.getElementById('password').value;
        // let user     = document.getElementById('usuario').value;
        // let roles    = document.getElementById('roles');
        // let role     = roles.options[roles.selectedIndex].text;

        // if (pnombre != "" && snombre != "" && apellido != "" && nid != "" && telefono != "" && email != "" && password != "" && user != "" && roles != "Seleccione un role") {

        // usuarios.registerUser(pnombre, snombre, apellido, nid, telefono, email, password, user, role);
        return usuarios.registerUser();
        // return false;

        // }
    });

    $('#registerClose').click(function(){
        usuarios.restablecerUser();
    });

    $('#deleteUser').click(function(){
        usuarios.deleteUser(data_User);
        data_User = null;
    });
});

var getUsers = (page)=>{
    let valor = document.getElementById('filtrarUser').value;
    usuarios.getUsers(valor, page);
}

var dataUser = (data) => {
    // console.log(data);
    usuarios.editUser(data);
}

var deleteUser = (data) => {
    document.getElementById("userName").innerHTML = data.email;
    data_User = data;
}

// -----------------------------------------------------------------------------------------------

/** Código de PRINCIPAL */

var principal = new Principal();

// -----------------------------------------------------------------------------------------------

/** Código de CLIENTES */
var pageTickets  = 0;
var pageClientes = 0;
var cliente      = new Clientes();

$(function () {
    $("#registerCliente").click(function () {
        return cliente.registerCliente();
    });

    $("#clienteClose").click(function () {
        cliente.restablecerCliente(1);
    });
});

var getCreditos = () => {
    cliente.getCreditos(null, 1);
}

var fotoCliente = (evt) => {
    cliente.archivo(evt, "fotoCliente");
}

var getClientes = (page) => {
    pageClientes = page;
    cliente.getClientes(page);
}

var getTickets = (page) => {
    pageTickets = page;
    cliente.getTickets(page);
}

var exportarTicketClientes = () => {
    cliente.exportarExcel(pageTickets, 1);
}

var exportarClientes = () => {
    cliente.exportarExcel(pageClientes,2);
}

// -----------------------------------------------------------------------------------------------

/* Código de Proveedores */
var pageTicketsp  = 0;
var pageProveedor = 0;
var proveedores   = new Proveedores();

$(function () {
    $("#registerProveedor").click( function () {
        return proveedores.registerProveedores();
    });
});

var fotoProvedor = (evt) => {
    proveedores.archivo(evt, "fotoProvedor");
}

var getProveedores = (page) => {
    pageProveedor = page;
    proveedores.getProveedores(page);
}

var dataProveedor = (email) => {
    proveedores.dataProveedor(email);
}

var getPtickets = (page) => {
    pageTicketsp = page;
    proveedores.getTickets(page);
}

var exportarTicketProveedores = () => {
    proveedores.exportarExcel(pageTicketsp,1);
}

var exportarProveedores = () => {
    proveedores.exportarExcel(pageProveedor,2);
}
// -----------------------------------------------------------------------------------------------
// Sección Compras
var compras =  new Compras();

var imageCompras = (evt) => {
    compras.archivo(evt, "imageCompras");
}

var getCompraProveedores= (page) =>{
    compras.getProveedores(page);
}

$(function () {
    $("#detallesCompras").click(function () {
        return compras.detallesComparas();
    });
   
});

// -----------------------------------------------------------------------------------------------

// Una vez la app está cargada en el navegador se ejecutan estos códigos:
$().ready(()=>{
    let URLactual = window.location.pathname;
    usuarios.userData(URLactual);
    principal.linkPrincipal(URLactual);

    $('#validate').validate();  // Todos los forms que tenan esta ID, se le validarán sus controles
    
    /*$('.sidenav').sidenav();    // Inicialización del menú para dispositivos móviles
    $('.modal').modal();
    $('select').formSelect();*/

    M.AutoInit();   // Iniciar controles Framework Materialize

});
