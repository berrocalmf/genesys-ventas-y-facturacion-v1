class Compras extends Uploadpicture {

    constructor() {
        super();
    }
    
    getProveedores(page) {
        $.post(
            URL + "Compras/getProveedores",
            { search: $("#searchCompraPD").val(), page: page },
            (response) => {
               // console.log(response);
                try {
                    let item = JSON.parse(response);
                    $("#compraProveedores").html(item.dataFilter);
                    $("#compraPD").html(item.paginador);
                } catch (error) {
                    $("#compraPD").html(response);
                }
            }
        );
    }

    dataProveedor(data){
        this.data = data;
        $("#Proveedor").val(data.Proveedor);
    }

    /**08.09.2020 Dev. fberrocalm */
    detallesComparas() {
        
        var valor =  true;

        if (this.data != null) {

            var data = new FormData();

            $.each($('input[type=file]')[0].files, (i, file) => {
                data.append('file', file);
            });

            var url     = "Compras/detallesCompras";
            var credito = document.getElementById("Credito").checked;

            data.append('Descripcion', $("#Descripcion").val());
            data.append('Cantidad',    $("#Cantidad").val());
            data.append('Precio',      $("#Precio").val());
            data.append('IdProveedor', this.data.IdProveedor);
            data.append('Proveedor',   $("#Proveedor").val());
            data.append('Email',       this.data.Email);
            data.append('Credito',     credito);

            $.ajax({
                url: URL + url,
                data: data,
                cache: false,
                contentType: false,
                processData: false,
                type: 'POST',
                success: (response) => {

                    if (response == 0) {
                        localStorage.setItem("Compra", JSON.stringify( new Array(
                            $("#Descripcion").val(),
                            $("#Cantidad").val(),
                            $("#Precio").val(),
                            this.data.Proveedor,
                            $("#Proveedor").val(),
                            this.data.Email,
                            credito
                        )));
                        window.location.href = URL + "Compras/detalles";
                    } else {
                        document.getElementById("messageCompras").innerHTML = response;
                    }

                }
            });

            valor =  false;

        } else {
            valor = true;
            document.getElementById("messageCompras").innerHTML = "Debe seleccionar el Proveedor";
        }

        return valor;

    }

}
