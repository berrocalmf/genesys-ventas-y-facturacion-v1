class Principal {
    constructor() {}

    linkPrincipal(link) {

        let url    = "";
        let cadena = link.split("/");

        for (let i = 0; i < cadena.length; i++) {
            if (i >= 3) {
                url += cadena[i];                
            }
        }

        switch (url) {
            case "principalprincipal":
                document.getElementById('enlace1').classList.add('active');
                break;
            case "Usuariosusuarios":
                document.getElementById('enlace2').classList.add('active');
                document.getElementById('files').addEventListener('change', archivo, false);
                document.getElementById('fotos').innerHTML = ['<img class="responsive-img" src="',PATHNAME + "resources/images/fotos/usuarios/default.png",'" title="',,'"/>'].join('');
                getRoles();
                getUsers(1);
                break;
            case "Clientesclientes":
                document.getElementById('fotoCliente').innerHTML = ['<img class="responsive-img" src="',PATHNAME + "resources/images/fotos/clientes/default.png",'" title="',,'"/>'].join('');
                getCreditos();
                document.getElementById('files').addEventListener('change', fotoCliente, false);
                getClientes(1);
                break;
            case "Clientesreportes":
                var email = getParameterByName('email');
                if (email != null) {
                    if(validarEmail(email)) {
                        new Clientes().getReporteCliente(email);
                    } else {
                        window.location.href = URL + "Clientes/clientes";
                    }
                } else {
                    window.location.href = URL + "Clientes/clientes";
                }
            case "Proveedoresproveedores":
                    getProveedores(1);
                    break;    
            case "Proveedoresregistrar":
                var email = getParameterByName('email'); // <-- tomamos un parámetro que viene por url (Parámetro de tipo GET)
                if (email != null && email != "") {
                    dataProveedor(email);
                }
                document.getElementById('files').addEventListener('change', fotoProvedor, false);
                break;
            case "Proveedoresreportes":
                var email = getParameterByName('email');
                if (email != null) {
                    if (validarEmail(email)) {
                        new Proveedores().getReporteProveedor(email);
                    } else {
                        window.location.href = URL + "Proveedores/proveedores";
                    }

                } else {
                    window.location.href = URL + "Proveedores/proveedores";
                }
                break;
            case "Comprascompras":
                new Compras().getProveedores(1);
                document.getElementById('files').addEventListener('change', imageCompras, false);
                break;    
            default:
                break;
        }

        /*
        if (link == PATHNAME+"principal/principal" || link == PATHNAME+"principal/principal/") {
            document.getElementById('enlace1').classList.add('active');
        }

        if (link == PATHNAME+"Usuarios/usuarios" || link == PATHNAME+"Usuarios/usuarios/") {
            document.getElementById('enlace2').classList.add('active');
            document.getElementById('files').addEventListener('change', archivo, false);
            document.getElementById('fotos').innerHTML = ['<img class="responsive-img" src="',PATHNAME + "resources/images/fotos/usuarios/default.png",'" title="',,'"/>'].join('');
            getRoles();
            getUsers(1);
        }

        if (link == PATHNAME+"Clientes/clientes" || link == PATHNAME+"Clientes/clientes/") {
            document.getElementById('fotoCliente').innerHTML = ['<img class="responsive-img" src="',PATHNAME + "resources/images/fotos/clientes/default.png",'" title="',,'"/>'].join('');
            getCreditos();
            document.getElementById('files').addEventListener('change', fotoCliente, false);
            getClientes(1);
        }
        */
        /*
            switch (link) {
            case PATHNAME+"principal/principal":
                document.getElementById('enlace1').classList.add('active');
                break;
            case PATHNAME+"Usuarios/usuarios":
                document.getElementById('enlace2').classList.add('active');
                break;    
        }
        */
    }
}
