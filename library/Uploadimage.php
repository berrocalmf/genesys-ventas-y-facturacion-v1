<?php 
    class Uploadimage {

        # Sube una imagen al servidor
        public function cargar_imagen($tipo, $imagen, $email, $carpeta) {
            $destino = "./resources/images/fotos/" . $carpeta . "/" . $email . ".png";

            if (strstr($tipo, "image")) {
                move_uploaded_file($imagen, $destino);
            } else {

                if(null == $imagen) {
                    $archivo = RQ . "images/fotos/".$carpeta."/default.png";
                } else {
                    $archivo = RQ . "images/fotos/" . $carpeta . "/" . $imagen;
                }

                // $archivo = RQ . "images/fotos/".$carpeta."/default.png";
                copy ($archivo, $destino);                                          // <-- Sobreescribe el archivo default con el email del usuario
            }
            return $email.".png";
        }
        
    }
     
?>
