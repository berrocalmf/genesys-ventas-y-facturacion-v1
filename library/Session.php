<?php 
    class Session {

        # Inicia variables de Session

        static function start() {
            @session_start();
        }

        # Retorna el valor almacenado en una variable de Session (Consulta una variable de sesión)

        static function getSession($name) {
            return $_SESSION[$name];
        }
        
        # Crea variables de Session

        static function setSession($name, $data) {
            return $_SESSION[$name] = $data;
        }

        # Destruye las variables de session

        static function destroy() {
            @session_destroy();
        }

    }
    
?>
