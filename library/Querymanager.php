<?php 
    /**
     * Clase para crear las Query que realizarán las consultas sobre las tablas de la base de datos
     */
    class Querymanager {
        private $pdo;
        private $lastId;

        // # Método constructor de la clase Querymanager

        public function __construct($USER, $PASS, $DB) {
            try {
                
                $this->pdo = new PDO('mysql:host=localhost;dbname='.$DB.';charset=utf8',$USER,$PASS,
                [
                    PDO::ATTR_EMULATE_PREPARES => false,
                    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
                ]
                );

            } catch (PDOException $e) {
                print "!Error¡: " . $e->getMessage();
                die();                                         
            }
        }

        // # Read Method number 1
        public function select1($fields, $table, $where, $params) {
            try {
                $where = $where ?? "";
                $query = "SELECT " . $fields . " FROM " . $table . $where;
                $sth = $this->pdo->prepare($query);
                $sth->execute($params);
                $response = $sth->fetchAll(PDO::FETCH_ASSOC);
                return array("results"=>$response);

            } catch (PDOException $e) {
                print "!Error¡: " . $e->getMessage();
            }

            $pdo = null;
        }

        // # Read Method number 2
        public function select2($attr, $table, $pagi_inicial, $pagi_cuantos, $where, $params) {
            try {
                $query = "SELECT " . $attr . " FROM " . $table . $where . " LIMIT $pagi_inicial,$pagi_cuantos";
                $sth = $this->pdo->prepare($query);
                $sth->execute($params);
                $response = $sth->fetchAll(PDO::FETCH_ASSOC);
                return array("results"=>$response);

            } catch (PDOException $e) {
                print "!Error¡: " . $e->getMessage();
            }

            $pdo = null;
        }

        # Función para insertar registros en las tablas

        public function insert($table, $param, $value) {
            try {
                $query = "INSERT INTO " . $table . $value;
                $sth   = $this->pdo->prepare($query);
                $sth->execute((array)$param);
                $this->lastId = $this->pdo->lastInsertId();
                return true;

            } catch (PDOException $e) {
                return $e->getMessage();
            }

            $pdo = null;
        }

        # Método para actualizar la información de una tabla

        public function update($table, $param, $value, $where) {
            try {
                $query = "UPDATE " . $table . " SET ".$value.$where;
                $sth   = $this->pdo->prepare($query);
                $sth->execute((array)$param);
                return true;
            } catch (PDOException $e) {
                return $e->getMessage();
            }

            $pdo = null;
        }

        public function delete($table, $where, $param) {
            try {
                $query = "DELETE FROM " . $table . $where;
                $sth   = $this->pdo->prepare($query);
                $sth->execute((array)$param);
                return true;
            } catch (PDOException $e) {
                return $e->getMessage();
            } 

            $pdo = null;
        }

        // Retorna el último Id insertado en una tabla. 

        public function getLastId() {
            return $this->lastId;
        }

    }
    
?>
