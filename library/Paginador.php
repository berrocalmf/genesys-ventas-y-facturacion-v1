<?php 
    class Paginador extends Conexion {
        
        public function __construct() {
            parent::__construct();
        }

        public function paginador($columns, $table, $method, $page, $where, $array) {

            $_pagi_enlace          = null;
            $_pagi_cuantos         = 6;                     // Cantidad de resultados por página
            $_pagi_nav_num_enlaces = 3;                     // Cantidad máxima de enlaces en la barra de navegación 
            $_pagi_mostrar_errores = false;                 // Si se muestran los errores de MySQL
            $_pagi_nav_anterior    = " &laquo; Anterior ";  // Definimos que irá en el enlace a la página anterior, podría ir a un tag <img> o a lo que sea      
            $_pagi_nav_siguiente   = " Siguiente &raquo ";  // Definimos que irá en el enlace a la página siguiente, podría ir a un tag <img> o a lo que sea      

            if(!isset($_pagi_nav_primera)) {
                $_pagi_nav_primera = " &laquo; Primero ";
            }

            if(!isset($_pagi_nav_ultima)) {
                $_pagi_nav_ultima = " Ultimo &raquo ";
            }

            if (empty($page)) {
                // Cuando se ejecuta el script la página por defecto será la primera, si no se ha seleccionado ninguna otra
                $_pagi_actual = 1;
            } else {
                // Si se seleccionó una página específica
                $_pagi_actual = $page;
            }
            
            // Consulta a la Base de datos
            $response = $this->db->select1($columns,$table,$where,$array); 
            
            if (is_array($response)) {
                $_pagi_results = $response["results"];
            } else {
                return $response;
            }

            $_pagi_totalReg            = count($_pagi_results);
            $_pagi_totalPags           = ceil($_pagi_totalReg/$_pagi_cuantos);
            $_pagi_navegacion_temporal = array(); // Para almacenar nuestra navegación

            if ($_pagi_actual != 1) {
                // Si no estamos en la página 1 ponemos el enlace primera
                $_pagi_url = 1; // Será el número de página la que enlazamos
                $_pagi_navegacion_temporal[] = "<a id='paginas1' href='#' onclick='" . "get" . $method . "(" . $_pagi_url . ")'>$_pagi_nav_primera</a>";

                // Si no estamos en la página 1 ponemos el enlace anterior
                $_pagi_url = $_pagi_actual - 1; // Será el número de página la que enlazamos
                $_pagi_navegacion_temporal[] = "<a id='paginas1' href='#' onclick='" . "get" . $method . "(" . $_pagi_url . ")'>$_pagi_nav_anterior</a>";
            }

            // $_pagi_nav_num_enlaces define cuantos enlaces con número de página se mostrarán como máximo
            if (!isset($_pagi_nav_num_enlaces)) {
                // Si no se definió se asume que se mostrarán todos los números de páginas
                $_pagi_nav_desde = 1;
                $_pagi_nav_hasta = $_pagi_totalPags;
            } else {
                $_pagi_nav_intervalo = ceil($_pagi_nav_num_enlaces / 2) - 1;
                $_pagi_nav_desde     = $_pagi_actual - $_pagi_nav_intervalo;
                $_pagi_nav_hasta     = $_pagi_actual + $_pagi_nav_intervalo;

                if ($_pagi_nav_desde < 1) {
                    $_pagi_nav_hasta -= ($_pagi_nav_desde - 1);
                    $_pagi_nav_desde  = 1; 
                }

                if ($_pagi_nav_hasta > $_pagi_totalPags) {
                    $_pagi_nav_desde -= ($_pagi_nav_hasta - $_pagi_totalPags); 
                    $_pagi_nav_hasta = $_pagi_totalPags;

                    if ($_pagi_nav_desde < 1) {
                        $_pagi_nav_desde = 1;
                    }
                }
            }

            for ($_pagi_i = $_pagi_nav_desde; $_pagi_i <= $_pagi_nav_hasta; $_pagi_i++) { 
                if ($_pagi_i == $_pagi_actual) {
                    $_pagi_navegacion_temporal[] = '<span id="paginas2">'.$_pagi_i.'</span>'; 
                } else {
                    $_pagi_navegacion_temporal[] = "<a id='paginas1' href='#' onclick='" . "get" . $method . "(" . $_pagi_i . ")'>" . $_pagi_i . "</a>";
                }
            }

            if ($_pagi_actual < $_pagi_totalPags) {
                $_pagi_url = $_pagi_actual + 1;
                $_pagi_navegacion_temporal[] = "<a id='paginas1' href='#' onclick='" . "get" . $method . "(" . $_pagi_url . ")'>" . $_pagi_nav_siguiente . "</a>";

                $_pagi_url = $_pagi_totalPags;
                $_pagi_navegacion_temporal[] = "<a class='waves-effect' href='#' onclick='" . "get" . $method . "(" . $_pagi_url . ")'>" . $_pagi_nav_ultima . "</a>";
            }

            // Obtener los registros que se mostrarán en la página actual
            $_pagi_navegacion = implode($_pagi_navegacion_temporal);
            $_pagi_inicial    = ($_pagi_actual - 1) * $_pagi_cuantos;
            $response         = $this->db->select2($columns, $table, $_pagi_inicial, $_pagi_cuantos, $where, $array);

            if (is_array($response)) {
                $_pagi_result2 = $response['results'];
            } else {
                return $response;
            }

            // Número del primer registro de la página actual
            $_pagi_desde = $_pagi_inicial + 1;
            $_pagi_hasta = $_pagi_inicial + $_pagi_cuantos;

            if ($_pagi_hasta > $_pagi_totalReg) {
                $_pagi_hasta = $_pagi_totalReg;
            }

            $_pagi_info = " del <b>$_pagi_desde</b> al <b>$_pagi_hasta</b> de <b>$_pagi_totalReg</b>";

            return array(
                "results"         => $_pagi_result2,
                "pagi_navegacion" => $_pagi_navegacion,
                "pagi_info"       => $_pagi_info
            );
            
        }
    }
?>
