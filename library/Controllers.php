<?php 
    class Controllers extends Anonymous {

        # constructor de la calse Controllers

        public function __construct() {
            // echo 'Genesys Sistema de Facturación Electrónica';
            Session::start();
            $this->view   = new Views();         // Esto es lo que se llama inyección o inyection
            $this->image  = new Uploadimage();   // Inyectamos una instancia de clase en otra
            $this->page   = new Paginador();     // Inyectamos una instancia de la clase Paginador
            $this->export = new ExportData();    // Inyectamos una instancia de la clase ExportData
            $this->loadClassModels();
        }
        
        # Funcionalidad para cargar modelos ().  Carga el modelo para la clase que está siendo instanciada

        public function loadClassModels() {
            $model = get_class($this).'_model';
            $path  = 'models/'.$model.'.php';

            if (file_exists($path)) {
                require_once $path;
                $this->model = new $model();
            }
        }
    }
    
?>
