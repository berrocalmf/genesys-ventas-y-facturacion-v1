<?php
    class Views {
        # Visualiza las plantillas de la vista del Controlador instanciado
        public function render($controller, $view, $models) {
            $controllers = get_class($controller);        

            require_once VIEWS.DFT.'header.html';
            if ($models == null) {
                require_once VIEWS.$controllers.'/'.$view.'.html';    
            } else {
                require_once VIEWS.$controllers.'/'.$view.'.php';
            }
            require_once VIEWS.DFT.'footer.html';
        }       
    }
?>
