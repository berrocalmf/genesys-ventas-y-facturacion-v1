<?php
    class Codigo {
        public static function Ticket($db, $table) {
            $count     = 0;
            $numTicket = 0;

            do {
                // gange: Retorna un array con los valores desde 1 hasta el valor máximo, dependiendo al rango ingresado ranfo inical 1 rango máximo: rand(1,10)
                $arrayTicket = range(1,  rand(1,10)); 

                foreach ($arrayTicket as $val){
                    $numTicket .= $val; 
                }

                $where    = " WHERE Ticket = :Ticket";
                $response = $db->select1("Ticket", $table, $where, array('Ticket' => (string)$numTicket));
                
                if(is_array($response)){
                    $response = $response['results'];
                    if (0 < count($response)) {
                        $count = count($response);
                    } else {
                        $count = 0;
                        return $numTicket;
                    } 
                }else{
                    $count = 0;        // break;  // Detiene el ciclo
                    return $response;
                }
                // echo var_dump($arrayTicket);
            } while (0 < $count);
        }

        /**
         * Genera numeros de tiket de tipo cadena
         * 10.08.2020 Dev. fberrocalm
         */
        public static function Tickets($db,$table,$propietario,$email) {
            $codigo    = null;
            $numTicket = null;
            $where     = " WHERE Propietario = :Propietario OR Email = :Email";
            $array = array(
                'Propietario' => $propietario,
                'Email'       => $email
            );

            $response = $db->select1("Ticket", $table, $where, $array);
            
            if (is_array($response)) {

                $response = $response['results'];

                if (0 == count($response)) {
                    return $numTicket = "0000000001";
                } else {
                    $data = end($response);  // Toma el último elemento de este array

                    if ("9999999999" == $data["Ticket"]) {
                        return $numTicket = "0000000001";
                    } else {
                        $cod = (int)  $data["Ticket"];
                        $cod++;
                        return $numTicket = str_pad($cod, 10, "0", STR_PAD_LEFT);
                    }
                }
                
            } else {
               return $response;
            }
        }

    }
?>
