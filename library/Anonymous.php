<?php 
    declare (strict_types = 1);
    class Anonymous {
        
        # Clase genérica utilizada para la tabla [usuarios]

        public function userClass($array) {
            return new Class($array) {
                public $pnombre;
                public $nid;
                public $snombre;
                public $apellidos;
                public $email;
                public $telefono;
                public $password;
                public $usuario;
                public $roles;
                public $imagen;

                function __construct($array) {
                    $this->nid       = $array[0];
                    $this->pnombre   = $array[1];
                    $this->snombre   = $array[2];
                    $this->apellidos = $array[3];
                    $this->telefono  = $array[4];
                    $this->email     = $array[5];
                    $this->password  = $array[6];
                    $this->usuario   = $array[7];
                    $this->roles     = $array[8];
                    $this->imagen    = $array[9];
                }
                
                public function getEmail() {
                    return $this->email;
                }
            };    
        }

        # Clase genérica utilizada para la tabla [clientes]

        public function clientesClass(array $array) {
            return new Class($array) {
                var $NID;
                var $Nombre;
                var $Apellido;
                var $Email;
                var $Direccion;
                var $Telefono;
                var $Creditos;

                function __construct($array) {
                    $this->NID       = $array[0];
                    $this->Nombre    = $array[1];
                    $this->Apellido  = $array[2];

                    if (is_numeric($array[3])) {
                        $this->Telefono  = $array[3];
                    }
                    
                    $this->Email     = $array[4];
                    $this->Direccion = $array[5];
                    $this->Creditos  = $array[6];
                }

            };
        }

        # Clase genérica utilizada para la tabla [reportes_clientes]

        public function reportClientesClass(array $array) {
            return new class($array) {
                var $Deuda;
                var $FechaDeuda;
                var $Pago;
                var $FechaPago;
                var $Ticket;
                var $IdClientes;

                function __construct($array) {
                    $this->Deuda      = $array[0];
                    $this->FechaDeuda = $array[1];
                    $this->Pago       = $array[2];
                    $this->FechaPago  = $array[3];
                    $this->Ticket     = $array[4];
                    $this->IdClientes = $array[5];
                }
                
            };
        }

        # Clase genérica utilizada para la tabla [ticket]
        
        public function ticketClass(array $array){
            return new class($array){
                var $Propietario;
                var $Deuda;
                var $FechaDeuda;
                var $Pago;
                var $FechaPago;
                var $Ticket;
                var $Email;

                function __construct($array){
                    $this->Propietario = $array[0];
                    $this->Deuda       = $array[1];
                    $this->FechaDeuda  = $array[2];
                    $this->Pago        = $array[3];
                    $this->FechaPago   = $array[4];
                    $this->Ticket      = $array[5];
                    $this->Email       = $array[6];
                }
            };
        }

        # Clase genérica utilizada para la tabla [proveedores]

        public function proveedoresClass(array $array){
            return new class($array){

                var $Proveedor;
                var $Telefono;
                var $Email;
                var $Direccion;
               
                // Constructor
                function __construct($array){
                    $this->Proveedor = $array[0];
                    $this->Telefono  = $array[1];
                    $this->Email     = $array[2];
                    $this->Direccion = $array[3];
                }
            };
        }

        # Clase genérica utilizada para la tabla [reportProveedores]

        public function reportProveedores(array $array){
            return new class($array){
                var $Deuda;
                var $FechaDeuda;
                var $Pago;
                var $FechaPago;
                var $Ticket;
                var $IdProveedor;

                // Constructor
                function __construct($array) {
                    $this->Deuda       = $array[0];
                    $this->FechaDeuda  = $array[1];
                    $this->Pago        = $array[2];
                    $this->FechaPago   = $array[3];
                    $this->Ticket      = $array[4];
                    $this->IdProveedor = $array[5];
                }
            };
        }

    }
    
?>
