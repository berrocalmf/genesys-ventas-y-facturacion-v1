<?php 
    require "config.php";

    $url = $_GET['url'] ?? 'Index/index'; // [ Captura la URL ]
    $url = explode('/', $url);  // ($url se convierte en un array separado por el símbolo /)

    $controller = '';
    $method     = '';
    $params     = '';

    // ---------------------------------------------------------
    // Se descompone la URL en Controlador, Método y Parámetros
    // ---------------------------------------------------------

    // Controller
    if(isset($url[0])) {
        $controller = $url[0];
    }

    // Method
    if(isset($url[1])) {
        if(isset($url[1]) != '') {
            $method = $url[1];
        }
    }

    // Si la posición 2 esta seteada tenemos un parámetro
    // Se captura el parámetro
    if(isset($url[2])) {
        if(isset($url[1]) != '') {
            $params = $url[2];
        }
    }

    // El sistema carga las clases de la carpeta /library que sean invocadas. 
    // Carga las clases que son invocadas y se van a utilizar automaticamente.
    // Para cargar las clases que están siendo invocadas desde este archivo.

    // Solo son cargadas las clases de la carpeta /[library].
    spl_autoload_register(function($class){
        if(file_exists(LBS.$class.'.php')) {
            require_once LBS.$class.'.php';
        }
    });

    // Se carga Controlador para manejo de Errores
    require_once 'controller/Errors.php';
    $error = new Errors();

    // $obj = new Controllers();    // echo $controller . ' ' . $method;
    
    $controllersPath = 'controller/'.$controller.'.php';                // <-- Variable que contien la ruta al Controlador solicitado

    if(file_exists($controllersPath)) {
        require_once $controllersPath;
        $contr = new $controller();

        if (isset($method)) {
            if (method_exists($controller, $method)) {
                if (isset($params)) {
                    $contr->{$method}($params);
                } else {
                    $contr->{$method}();
                }
            } else {
                $error->error();
            }
        }
    }
    
    if(!file_exists($controllersPath)) {
        $error->error();
    }    

?>
