<?php 
    # Modelo de Index
    class Index_model extends Conexion {

        public function __construct() {
            parent::__construct();
        }

        # Procedimiento para login de usuarios    
        public function userLogin($email, $password) {
            $where    = " WHERE email = :email";
            $param    = array('email' => $email);
            $response = $this->db->select1("*", 'usuarios', $where, $param);
            //var_dump($response);
            if(is_array($response)) {

                $response = $response['results'];    
                if(0 != count($response)) {

                    if (password_verify($password,$response[0]['password'])) {
                        $data = array(
                            "idUsuario" => $response[0]['idUsuario'],
                            "pnombre" => $response[0]['pnombre'],
                            "apellidos" => $response[0]['apellidos'],
                            "roles" => $response[0]['roles'],
                            "imagen" => $response[0]['imagen']
                        );

                        Session::setSession("User", $data);

                        return $data;
                    } else {
                        $data = array(
                            "idUsuario" => 0
                        );
                        
                        return $data;
                    }

                } else {
                    return "Usuario no registrado";
                }
                
            } else {
                return $response;
            }

        }
        
    }
    
?>
