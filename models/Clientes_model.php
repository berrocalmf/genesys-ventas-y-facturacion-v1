<?php 
    class Clientes_model extends Conexion {

        public function __construct() {
            parent::__construct();
        }

        # Listado de todos los límites de crédito creados (Ojo con los SELECT ALL)

        public function getCreditos() {
            return $this->db->select1("*", "creditos", null, null);
        }

        # Registro de Clientes (Create)

        public function registerCliente($cliente, $r_cliente) {
            $where    = " WHERE Email = :Email";
            $response = $this->db->select1("*",'clientes',$where,array('Email' => $cliente->Email));

            if (is_array($response)) {

                $response = $response['results'];

                if (0 == count($response)) {
                    
                    // INSERCIÓN MAESTRO DETALLE

                    $value  = "(NID,Nombre,Apellido,Email,Telefono,Direccion,Creditos)";
                    $value .= " VALUES(:NID,:Nombre,:Apellido,:Email,:Telefono,:Direccion,:Creditos)";

                    $data   = $this->db->insert("clientes",$cliente,$value);

                    if (is_bool($data)) {
                        // SI SE CREO EL CLIENTE, SE INSERTA EL DETALLE RELACIONAL
                        $response = $this->db->select1("*",'clientes',$where,array('Email' => $cliente->Email));
                        if (is_array($response)) {
                            $response = $response['results'];
                            $r_cliente->IdClientes = $response[0]["IdClientes"]; // <-- Accediendo a propiedad de un objeto 

                            $value  = "(Deuda,FechaDeuda,Pago,FechaPago,Ticket,IdClientes)";
                            $value .= " VALUES(:Deuda,:FechaDeuda,:Pago,:FechaPago,:Ticket,:IdClientes)";

                            $data   = $this->db->insert("reportes_clientes",$r_cliente,$value);
                            if (is_bool($data)) {
                                return 0;
                            } else {
                                return $data;
                            }    


                        } else {
                            return $data;
                        }    
                        
                    } else {
                        return $data;
                    }

                } else {
                    return 1;  // El cliente ya está registrado
                }
                
            } else {
                return $response;  // El query no se ejecutó correctamente
            }
            
        }

        # Retorna el listado de clientes
        # 29.04.2020 Dev. FMBM 

        public function getClientes($filter, $page, $model) {
            $where = " WHERE NID LIKE :NID OR Nombre LIKE :Nombre OR Apellido LIKE :Apellido";
            $array = array(
                'NID'      => '%'.$filter.'%',
                'Nombre'   => '%'.$filter.'%',
                'Apellido' => '%'.$filter.'%'
            );

            $columns = "IdClientes,NID,Nombre,Apellido,Email,Telefono,Direccion,Creditos";
            return $model->paginador($columns,"clientes","Clientes",$page,$where,$array);  // Uso el modelo que se pasa cómo parámetro
        }

        # Muestra reporte de estado de cartera del cliente
        # 04.05.2020 Dev. FMBM

        public function getReporteCliente($email) {
            $where    = " WHERE Email = :Email";
            $response1 = $this->db->select1("*", 'clientes', $where, array('Email' => $email)); 

            if (is_array($response1)) {
                $response1 = $response1['results'];
                
                if (0 != count($response1)) {

                    $where     = " WHERE IdClientes = :IdClientes";
                    $response2 = $this->db->select1("*",'reportes_clientes',$where,array('IdClientes' => $response1[0]["IdClientes"]));

                    if (is_array($response2)) {
                        $response2 = $response2['results'];

                        if (0 != count($response2)) {

                            $data = array(
                                "Nombre"     => $response1[0]["Nombre"],
                                "Apellido"   => $response1[0]["Apellido"],
                                "Email"      => $response1[0]["Email"],
                                "Creditos"   => $response1[0]["Creditos"],
                                "IdReportes" => $response2[0]["IdReportes"], 
                                "Deuda"      => $response2[0]["Deuda"], 
                                "FechaDeuda" => $response2[0]["FechaDeuda"], 
                                "Pago"       => $response2[0]["Pago"], 
                                "FechaPago"  => $response2[0]["FechaPago"], 
                                "Ticket"     => $response2[0]["Ticket"], 
                                "IdClientes" => $response2[0]["IdClientes"]
                            );
                            Session::setSession("reportCliente", $data);
                            return $data;

                        } else {
                            return 0;
                        }

                    } else {
                        return $response2;
                    }    

                    return $data;
                } else {
                    // return 'Email no registrado.';
                    return 0;
                }
                
            } else {
                return $response1;
            }
        }

        /**
         * Registrar pagos o abonos de un cliente en la Base de Datos
         * 23.06.2020 - Dev. FMBM
         */
        public function setPagos($model1,$model2,$idReporte){

            $Ticket = Codigo::Ticket($this->db, "ticket");    

            if (is_numeric($Ticket)) {
                $value =  "Deuda = :Deuda,FechaDeuda = :FechaDeuda,Pago = :Pago,FechaPago = :FechaPago,Ticket = :Ticket,IdClientes = :IdClientes";
                $where = " WHERE IdReportes = ".$idReporte;

                $model1->Ticket = (string)$Ticket;
                $data = $this->db->update("reportes_clientes", $model1, $value, $where);

                if (is_bool($data)) {

                    $value          = " (Propietario,Deuda,FechaDeuda,Pago,FechaPago,Ticket,Email) VALUES (:Propietario,:Deuda,:FechaDeuda,:Pago,:FechaPago,:Ticket,:Email)";
                    $model2->Ticket = (string)$Ticket;
                    $data           =  $this->db->insert("ticket", $model2, $value);

                    if (is_bool($data)) {
                        return 0;
                    } else {
                        return $data;
                    }

                } else {
                    return $data;
                }
    
            } else {
                return $Ticket;
            }

        }

        /**
         * Actualizar los datos del cliente (Edit y Update)
         * 28.06.2020 - Dev. FMBM
         */

        public function editCliente($idCliente,$cliente){

            // 1. Se busca un cliente con el email que se desee actualizar (Para determinar si ya está registrado)

            $where = " WHERE Email = :Email";
            $response = $this->db->select1("*",'clientes',$where,array('Email' => $cliente->Email));

            if (is_array($response)) {

                // Se realizó la consulta de forma correcta.

                $response = $response['results'];
                $value    =  "NID = :NID,Nombre = :Nombre,Apellido = :Apellido,Email = :Email,Telefono = :Telefono,Direccion = :Direccion,Creditos = :Creditos";
                $where    = " WHERE IdClientes = ".$idCliente;
                
                if (0 == count($response)) {

                    // 2. El email que se desea actualizar no está registrado.  (Se procede a actualizar)

                    $data = $this->db->update("clientes",$cliente,$value, $where);

                    if (is_bool($data)) {
                        return 0;
                    } else {
                        return  $data;
                    }

                } else {

                    // 3. Ya existe el email en la Base de datos

                    if ($response[0]['IdClientes'] == $idCliente) {

                        // 4. Si el email corresponde al mismo cliente que se desea actualizar. (Se procede a actualizar)

                        $data = $this->db->update("clientes",$cliente,$value, $where);

                        if (is_bool($data)) {
                            return 0;
                        } else {
                            return  $data;
                        }

                    }else{

                        // 5. El email ya está registrado por otro Cliente.

                        return 1;
                    }
                }
                
            } else {

               return $response;

            }
        }

        /**
         * Retorna los tickets de un cliente
         * 28.06.2020 Dev. FMBM
         * $filter: Filtro
         * $page: Página
         * $model: Paginador
         */

        public function getTickets($filter, $page, $model){
            $where = " WHERE Propietario = :Propietario AND Email LIKE :Email ";
            
            $array = array(
                'Propietario' => "Cliente",
                'Email'       => '%'.$filter.'%'
            );

            return $model->paginador("*", "ticket", "Tickets", $page, $where, $array);
        }

    }    
?>
