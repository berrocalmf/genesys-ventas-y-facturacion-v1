<?php 
    class Usuarios_model extends Conexion {

        public function __construct() {
            parent::__construct();
        }

        # Listado de todos los roles creados (Ojo con los SELECT ALL)

        function getRoles() {
            return $response = $this->db->select1("*", "roles", null, null);
        }

        # Registro de usuarios nuevos (create)
        
        public function registerUser($user) {
            $where = " WHERE email = :email";
            // $param = array('email'=>$emailIn);
            $param = array('email'=>$user->getEmail());
            $response = $this->db->select1("*", 'usuarios', $where, $param);

            if ( is_array($response) ) {
                $response = $response['results'];
                
                if (0 == count($response)) {
                    // No está registrado.  (Realizo el registro).

                    $value  = "(nid,pnombre,snombre,apellidos,email,telefono,password,usuario,roles,imagen)";
                    $value .= " VALUES(:nid,:pnombre,:snombre,:apellidos,:email,:telefono,:password,:usuario,:roles,:imagen)";

                    $data = $this->db->insert("usuarios", $user, $value);

                    if (is_bool($data)) {
                        return 0;
                    } else {
                        return $data;
                    }
                    // return var_dump((Array)$user);
                } else {
                    return 1; // Ya está registrado
                }

            } else {
                return $response;   // <-- En este caso la respuesta no fueron datos.
            }

        }

        # Para realizar el filtro o búsquedas el la grilla de usuarios
        public function getUsers($filter, $page, $model) {
            $where = " WHERE nid LIKE :NID OR pnombre LIKE :Nombre OR apellidos LIKE :Apellidos";

            $array = array(
                'NID' => '%' . $filter . '%',
                'Nombre' => '%' . $filter . '%',
                'Apellidos' => '%' . $filter . '%'
            );

            $columns = 'idUsuario,nid,pnombre,snombre,apellidos,email,telefono,usuario,roles,imagen';
            return $model->paginador($columns,"usuarios","Users",$page,$where,$array);
        }

        # Actualiza la informaicón  del usuario seleccionado
        public function editUsers($user, $idUsuario) {
            $where = " WHERE email = :email";
            $response = $this->db->select1("*", 'usuarios', $where, array('email' => $user->email));

            if (is_array($response)) {

                $response = $response['results'];

                $value = "nid = :nid, pnombre = :pnombre, snombre = :snombre, apellidos = :apellidos, telefono = :telefono, email = :email, password = :password, usuario = :usuario, roles = :roles, imagen = :imagen";
                $where = " WHERE idUsuario=" . $idUsuario;

                if (0 == count($response)) {
                    $data = $this->db->update("usuarios",$user,$value,$where);
                    if(is_bool($data)) {
                        return 0;
                    } else {
                        return $data;
                    }
                } else {
                    if ($response[0]['idUsuario'] == $idUsuario) {
                        $data = $this->db->update("usuarios",$user,$value,$where);
                        if(is_bool($data)) {
                            return 0;
                        } else {
                            return $data;
                        }
                    } else {
                        return "El email ya está registrado";
                    }
                }
            } else {
                return $response;
            }

        }

        # Consulta información de un id de usuario
        public function getUser($idUsuario) {

            $where = " WHERE idUsuario = :idUsuario";
            $response = $this->db->select1("*", 'usuarios', $where, array('idUsuario'=>$idUsuario));

            if ( is_array($response) ) {
                return $response = $response['results'];
            } else {
                return $response;
            }
        }
        
        # Elimina un usuario de la base de datos
        public function deleteUser($idUsuario, $email) {
            $where = " WHERE idUsuario = :IdUsuario";
            $data  = $this->db->delete('usuarios',$where,array('IdUsuario' => $idUsuario));

            if (is_bool($data)) {
                $archivo = RQ . "images/fotos/usuarios/" . $email . ".png";
                unlink($archivo);
                return 0;
            } else {
                return $data;
            }
            
        }

    }
    
?>
