<?php
    class Compras_model extends Conexion{

        function __construct(){
            parent::__construct();
        }

        function getProveedores($filter,$page,$model){
            
            $where = " WHERE Proveedor LIKE :Proveedor OR Email LIKE :Email";
            $array = array(
                'Proveedor' => '%'.$filter.'%',
                'Email' => '%'.$filter.'%'
            );
            
            return $model->paginador("*","proveedores","CompraProveedores",$page,$where,$array);
        }

    }
?>
