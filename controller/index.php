<?php
    # Controlador Index
    class Index extends Controllers {
        
        public function __construct() {
            // echo 'Controlador Index';
            parent::__construct();
        }

        public function index() {
            // echo 'Metodo index() del controlador Index';
            $user = $_SESSION["User"] ?? null; 
            if (null != $user) {
                header("Location:".URL."Principal/principal");
            } else {
                $this->view->render($this, 'index', null);
            }
            
        }

        # Función para el logueo de usuarios
        
        public function userLogin() {
            // if(isset($_POST['email']) && isset($_POST['password'])) {
            if(isset($_POST['email'])) {  
                if (!empty($_POST['password'])) {
                    if (8 <= strlen($_POST['password'])) {
                        $data = $this->model->userLogin($_POST['email'], $_POST['password']);
                        if (is_array($data)) {
                            echo json_encode($data);        // <-- Se manda la data resultado del proceso de login (datos del usuario logueado)
                        } else {
                            echo $data;                     // <-- Se manda la data resultado del proceso de login (Mensage de error)
                        }
                    } else {
                        echo 2;                       
                    }
                } else {
                    echo 1;                    
                }
            }
        }
    }
?>
