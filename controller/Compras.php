<?php
    class Compras extends Controllers {

        function __construct() {
            parent::__construct();
        }

        /**30.08.2020 Dev. fberrocalm */
        public function productos() {
            $user = Session::getSession("User");
            if (null != $user) {

                if ("Admin" == $user["roles"]) {
                    $this->view->render($this,"productos",null);
                } else {
                    header("Location:" . URL . "Principal/principal");
                }

            } else {
                header("Location:".URL);
            }
        }

        /**31.08.2020 Dev. fberrocalm */
        public function compras() {
            $user = Session::getSession("User");

            if (null != $user) {

                if ("Admin" == $user["roles"]) {
                    $this->view->render($this,"compras",null);
                } else {
                    header("Location:".URL."Principal/principal");
                }

            } else {
                header("Location:".URL);
            }
        }

        public function getProveedores(){
       
            $user = Session::getSession("User");
            if (null != $user) {

                $count      = 0;
                $dataFilter = null;
                $data       = $this->model->getProveedores($_POST["search"],$_POST["page"],$this->page);

                if (is_array($data)) {
                    $array = $data["results"];

                    foreach ($array as $key => $value) {
                        $dataProveedor = json_encode($array[$count]);    
                        $urlImage = URL."resources/images/fotos/proveedores/" . $value["Email"] . ".png";
                        $url      = URL."Proveedores/reportes/?email=" . $value["Email"]; // Esta variable no se está usando
                    
                        $dataFilter .= "<tr>" .
                            "<td >".
                                "<ul class='collection'>".
                                    "<li class='collection-item avatar'>".
                                        "<img class='responsive circle' src='" . $urlImage . "'/>
                                    </li>
                                </ul>
                            </td>".
                            "<td>" . $value["Proveedor"] . "</td>".
                            "<td>".
                                "<a onclick='compras.dataProveedor(" . $dataProveedor . ")' class='btn-small'>Seleccionar</a>".
                            "</td>".
                        "</tr>";
                        $count++;
                    }

                    $paginador = "<p>Resultados " . $data["pagi_info"] . "</p><p>" . $data["pagi_navegacion"] . "</p> ";
                    echo json_encode(array(
                        "dataFilter" => $dataFilter,
                        "paginador"  => $paginador
                    ));

                } else {
                    echo $data;
                }

            }
        }

        /** 08.09.2020 Dev fberrocalm */
        public function detallesCompras() {

            $user = Session::getSession("User");

            if(null != $user) {
                if ("Admin" == $user["roles"]) {

                    if (empty($_POST["Descripcion"])) {
                        echo "El campo Descripcion es obligatorios";
                    } else {
                        if (empty($_POST["Precio"])) {
                            echo "El campo Precio es obligatorios";
                        } else {

                            if (isset($_FILES['file'])) {
                                $_FILES = $_FILES["file"];
                            } else {
                                $_FILES = NULL;
                            }

                            Session::setSession("Compra",array(
                                $_POST["Descripcion"],
                                $_POST["Cantidad"],
                                $_POST["Precio"],
                                $_POST["IdProveedor"],
                                $_POST["Proveedor"],
                                $_POST["Email"],
                                $_POST["Credito"],
                                $_FILES,
                            ));

                            echo 0;
                        }
                    }
    
                }
            }
        }

        public function detalles() {
            
        }

    }
?>
