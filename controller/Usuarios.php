<?php 
    class Usuarios extends Controllers {
        
        public function __construct() {
            parent::__construct();
            // $this->getRoles();
        }

        # Muestra la vista de usuarios
        
        public function usuarios() {
            if (null != Session::getSession("User")) {
                $this->view->render($this, "usuarios", null);
            } else {
                header("Location:".URL);
            }
        }

        #   Listado de todos los roles creados

        public function getRoles() {
            $data = $this->model->getRoles();

            if(is_array($data)) {
                echo json_encode($data);        
            } else {
                echo $data;
            }
        }

        # función para crear usuarios

        public function registerUser () {
            $user = Session::getSession("User");

            if ($user != null) {
                if ($user["roles"]=="Admin") {
                    
                    if (empty($_POST["pnombre"])) {         //nid
                        echo 'El campo pnombre es obligatorio';
                    } else {
                        
                        if (empty($_POST["apellido"])) {
                            echo 'El campo apellidos es obligatorio';
                        } else {    
                            
                            if (empty($_POST["nid"])) {
                                echo 'El campo NID es obligatorio';
                            } else {
                                
                                if (empty($_POST["telefono"])) {        //telefono
                                    echo 'El campo telefono es obligatorio';
                                } else {
                                
                                    if (empty($_POST["password"])) {
                                        echo 'El campo password es obligatorio';
                                    } else {

                                        if(strlen($_POST["password"]) >= 8 ) {

                                            if (empty($_POST["usuario"])) {
                                                echo 'El campo usuario es obligatorio';
                                            } else {
                                                if (strcmp("Seleccione un role", $_POST["roles"]) === 0) {
                                                    // Seleccione un role
                                                    echo 'Seleccione un role';
                                                } else {
                                                    
                                                    $archivo = null;
                                                    $tipo    = null;

                                                    // Obtenemos datos del archivo tipo imagen pasado por POST desde el cliente
                                                    // if (isset($_FILES['file'])) {
                                                    //     $tipo    = $_FILES['file']["type"];
                                                    //     // $imagen  = $_FILES['file']["type"];
                                                    //     $archivo = $_FILES['file']["tmp_name"];  
                                                    //     // $imagen = $this->model->cargar_imagen($tipo, $archivo, $_POST['email']); 
                                                    // } 
                                                    
                                                    // $imagen = $this->image->cargar_imagen($tipo, $archivo, $_POST['email'],"usuarios"); 

                                                    //else {
                                                    //   $imagen = "default.png";
                                                    //}

                                                    $array = array(
                                                        $_POST['nid'], $_POST['pnombre'], $_POST['snombre'], $_POST['apellido'], $_POST['telefono'],
                                                        $_POST['email'], password_hash($_POST['password'], PASSWORD_DEFAULT), $_POST['usuario'],
                                                        $_POST['roles'], $_POST['email'].".png"
                                                    );
                                                    // $data = $this->model->registerUser($_POST['pnombre'], $_POST['snombre'], $_POST['apellido'], $_POST['nid'], $_POST['telefono'], $_POST['email'], $_POST['password'], $_POST['usuario'], $_POST['roles']);
                                                    
                                                    $data = $this->model->registerUser($this->userClass($array));
                                                    if ($data == 1) {
                                                        echo 'Email ya ha sido registrado';
                                                    } else {
                                                        // if ($data != 0) {
                                                        //     echo $data;
                                                        // }
                                                        if ($data == 0) {
                                                            if (isset($_FILES['file'])) {
                                                                $tipo =  $_FILES['file']["type"];
                                                                $archivo =  $_FILES['file']["tmp_name"];
                                                               
                                                            }

                                                            // $imagen = $this->image->cargar_imagen($tipo, $archivo, $_POST['email'],"usuarios");
                                                            $this->image->cargar_imagen($tipo,$archivo,$_POST["email"],"usuarios");
                                                            echo 0; // Para indicar que la información del usuario se ingresó correctamente

                                                        } else {
                                                            echo $data;
                                                        }

                                                    }

                                                }
                                                
                                            }

                                        } else {
                                            echo 'Ingrese contraseña de 8 dígitos o más';
                                        }
                                         
                                    }

                                }

                            }

                        }

                    }
                    

                } else {
                    echo "No tiene autorización";
                }
            }
        }

        # Listado de usuarios segun parámetro de filtrado

        public function getUsers() {
            $user = Session::getSession("User");
            if ($user != null) {
                // echo $_POST['filter'];
                $dataFilter = null;
                $data       = $this->model->getUsers($_POST['filter'], $_POST['page'], $this->page); // le pasamos el filter la page y le objeto page de tipo pagonador

                if (is_array($data)) {
                    $count = 0;
                    $array = $data['results'];
                    foreach ($array as $key => $value) {
                        $dataUser = json_encode($array[$count]);
                        $urlImage = URL . "resources/images/fotos/usuarios/" .  $value["imagen"];

                        $dataFilter .= "<tr>" .
                            // "<td>".$value["nid"]        ."</td>" .
                            "<td>" .
                                "<ul class='collection'>" .
                                    "<li class='collection-item avatar'>" .
                                        "<img class='responsive circle' src='" . $urlImage . "' />" .
                                    "</li>" .
                                "</ul>" .
                            "</td>" .
                            "<td>".$value["pnombre"]    ."</td>" .
                            "<td>".$value["usuario"]    ."</td>" .
                            "<td>".$value["roles"]      ."</td>" .
                            "<td>".
                            // "<a href='#modal1' onclick='dataUser(".$dataUser.")' class='btn modal-trigger'>Edit</a> | " . 
                            "<a href='#modal1' onclick='dataUser(".$dataUser.")' class='modal-trigger'><i title='Editar' class='fas fa-pen' style='color: cadetblue;'></i></a> &nbsp;" . 
                            // "<a href='#modal2' onclick='deleteUser(".$dataUser.")' class='btn red lighten-1 modal-trigger'>Delete</a>" . 
                            "<a href='#modal2' onclick='deleteUser(".$dataUser.")' class='modal-trigger'><i title='Eliminar' class='fas fa-trash' style='color: cadetblue;'></i></a>" . 
                            "</td>".
                            "</tr>";
                        $count++;
                    }

                    $paginador = "<p>Resultados " . $data["pagi_info"] . "</p><p>" . $data["pagi_navegacion"] . "</p>";
                    echo json_encode( array(
                        "dataFilter" => $dataFilter,
                        "paginador"  => $paginador
                    ));
                    //echo $dataFilter; // Response del servidor

                } else {
                    echo $data;   
                }
            }
        }

        # Edición de la información del usuario

        public function editUser() {

            $user = Session::getSession("User");

            if ($user != null) {
                if ($user["roles"]=="Admin") {
                    
                    if (empty($_POST["pnombre"])) {         //nid
                        echo 'El campo pnombre es obligatorio';
                    } else {
                        
                        if (empty($_POST["apellido"])) {
                            echo 'El campo apellidos es obligatorio';
                        } else {    
                            
                            if (empty($_POST["nid"])) {
                                echo 'El campo NID es obligatorio';
                            } else {
                                
                                if (empty($_POST["telefono"])) {        //telefono
                                    echo 'El campo telefono es obligatorio';
                                } else {
                                
                                    if (empty($_POST["password"])) {
                                        echo 'El campo password es obligatorio';
                                    } else {

                                        if(strlen($_POST["password"]) >= 8 ) {

                                            if (empty($_POST["usuario"])) {
                                                echo 'El campo usuario es obligatorio';
                                            } else {
                                                if (strcmp("Seleccione un role", $_POST["roles"]) === 0) {
                                                    echo 'Seleccione un role';
                                                } else {
                                                    
                                                    $imagen  = null;
                                                    $archivo = null;
                                                    $tipo    = null;

                                                    // Condicional para definir la imagen usada por el usuario del sistema:
                                                    if (isset($_FILES['file'])) {
                                                        $tipo    = $_FILES['file']["type"];
                                                        $archivo = $_FILES['file']["tmp_name"];
                                                        $imagen = $this->image->cargar_imagen($tipo, $archivo, $_POST['email'], "usuarios");
                                                    } else {
                                                        if (isset($_POST["imagen"])) {
                                                            // $imagen = $_POST["imagen"];                     //  . ".png";
                                                            $archivo = $_POST["imagen"];
                                                            $imagen  = $this->image->cargar_imagen($tipo, $archivo, $_POST['email'], "usuarios");

                                                            if($_POST["imagen"] != $_POST['email'] . ".png") {
                                                                $archivo = RQ . "images/fotos/usuarios/" . $archivo;
                                                                unlink($archivo);
                                                                $archivo = null;
                                                            }
                                                        } 
                                                    }

                                                    $response = $this->model->getUser($_POST["idUsuario"]);

                                                    // echo "<script>console.log(".var_dump($response).");</script>";

                                                    if (is_array($response)) {
                                                        $array = array(
                                                            $_POST['nid'],$_POST['pnombre'],$_POST['snombre'],$_POST['apellido'],$_POST['telefono'],$_POST['email'],$response[0]['password'],$_POST['usuario'],$_POST['roles'],$imagen
                                                        );
                                                        echo $this->model->editUsers($this->userClass($array),$_POST["idUsuario"]);                
                                                    } else {
                                                        echo $response;
                                                    }

                                                }
                                                
                                            }

                                        } else {
                                            echo 'Ingrese contraseña de 8 dígitos o más';
                                        }
                                         
                                    }

                                }

                            }

                        }

                    }

                } else {
                    echo "No tiene autorización";
                }
            }

        }

        # Eliminar usuarios de la Base de Datos

        public function deleteUsers() {
            $user = Session::getSession("User");
            if ($user != null) {
                if ($user["roles"]=="Admin") {
                    echo $this->model->deleteUser($_POST['idUsuario'], $_POST['email']);
                } else {
                    echo "No tiene autorización";
                }
            }        
        }

        # Procedimiento para destruir Sessiones

        public function destroySession() {
            Session::destroy();
            header("Location:".URL);
        }

    }
    
?>
