<?php 
    class Clientes extends Controllers {

        private $archivo = null;
        private $tipo    = null;

        public function __construct() {
            parent::__construct();
        }

        public function clientes() {
            
            if (null != Session::getSession("User")) {
                $this->view->render($this, "clientes", null);    
            } else {
               header("Location:" . URL);
            }
            
        }

        # Obtener listado de creditos (llave foránea de Clientes)
        public function getCreditos() {
            $user = Session::getSession("User");

            if ($user != null) {
                $data = $this->model->getCreditos();

                if (is_array($data)) {
                    echo json_encode($data);
                } else {
                    echo $data;
                }
            }   
        }

        # Registro de clientes

        public function registerCliente() {
            $user = Session::getSession("User");

            if ($user != null) {

                if ($user["roles"]=="Admin") {

                    if (empty($_POST["nombre"])) {
                        echo 'El campo nombre es obligatorio.';
                    } else {

                        if (empty($_POST["apellido"])) {
                            echo 'El campo apellido es obligatorio.';
                        } else {

                            if (empty($_POST["nid"])) {
                                echo 'El campo nid es obligatorio.';
                            } else {

                                if (empty($_POST["telefono"])) {
                                    echo 'El campo tel&eacute;fono es obligatorio.';
                                } else {

                                    if (empty($_POST["email"])) {
                                        echo 'El campo email es obligatorio.';
                                    } else {

                                        if (empty($_POST["direccion"])) {
                                            echo 'El campo dirección es obligatorio.';
                                        } else {

                                            // if (isset($_FILES['file'])) {
                                            //     $this->tipo    = $_FILES['file']["type"];
                                            //     $this->archivo = $_FILES['file']["tmp_name"];  
                                            // }
                                            // $this->image->cargar_imagen($this->tipo, $this->archivo, $_POST['email'],"clientes"); 

                                            $array1 = array(
                                                $_POST["nid"],$_POST["nombre"],$_POST["apellido"],$_POST["telefono"],$_POST["email"],$_POST["direccion"],$_POST["creditos"]
                                            );

                                            $array2 = array(
                                                "$0.00","--/--/--","$0.00","--/--/--","000000",0
                                            );

                                            $data = $this->model->registerCliente($this->clientesClass($array1),$this->reportClientesClass($array2));
                                            // var_dump($this->clientesClass($array));
                                            if ($data == 1) {
                                                echo 'Email ' .$_POST["email"]. ' ya registrado.';
                                            } else {

                                                if ($data == 0){

                                                    if(isset($_FILES['file'])){
                                                        $this->tipo =  $_FILES['file']["type"];
                                                        $this->archivo =  $_FILES['file']["tmp_name"];
                                                    }

                                                    $this->image->cargar_imagen($this->tipo,$this->archivo,$_POST["email"],"clientes");
                                                    echo 0; // Para indicar que la información del usuario se ingresó correctamente
                                                }else{
                                                    echo $data;
                                                }
                                                
                                            }
                                            
                                        }
                                        
                                    }
                                    
                                }
                                
                            }
                            
                        }
                        
                    }

                } else {
                    echo "No tiene autorización";
                }

            }    
        }

        # Retorna listado de clientes
        # 29.04.2020 - Dev. FMBM

        public function getClientes() {
            $user = Session::getSession("User");

            if ($user != null) {
                $count      = 0;
                $dataFilter = null;

                $data       = $this->model->getClientes($_POST["search"],$_POST["page"],$this->page);
                // echo var_dump($data);
                if (is_array($data)) {
                    $array = $data["results"];

                    foreach ($array as $key => $value) {
                        $dataCliente = json_encode($array[$count]); 
                        $urlImage    = URL."resources/images/fotos/clientes/".$value['Email'].".png";

                        // $url = URL."Clientes/reportes/".$value['Email'];
                        if ($user["roles"]=="Admin") {
                            $botonReportes = "<a href='".URL."Clientes/reportes/?email=".$value['Email']."' class='modal-trigger'><i title='Deuda' class='fas fa-file-invoice-dollar' style='color: cadetblue;'></i></a> &nbsp;&nbsp;";
                            // $botonReportes = "<a href='".$url."' class='modal-trigger'><i title='Reportes' class='fas fa-trash' style='color: cadetblue;'></i></a> &nbsp;&nbsp;";
                        } else {
                            $botonReportes = "";
                        }

                        $dataFilter .= "<tr>" .
                        "<td>" .
                            "<ul class='collection'>" .
                                "<li class='collection-item avatar'>" .
                                    "<img class='responsive circle' src='" . $urlImage . "' />" .
                                "</li>" .
                            "</ul>" .
                        "</td>" .
                        "<td>".$value["Nombre"]    ."</td>" .
                        "<td>".$value["Apellido"]  ."</td>" .
                        "<td>".
                        // "<a href='#modal2' onclick='deleteUser(".$dataUser.")' class='btn red lighten-1 modal-trigger'>Delete</a>" . 
                        $botonReportes . 
                        // "<a href='#modal1' onclick='dataUser(".$dataUser.")' class='btn modal-trigger'>Edit</a> | " . 
                        "<a href='#modal1' onclick='cliente.dataCliente(".$dataCliente.")' class='modal-trigger'><i title='Editar' class='fas fa-pen' style='color: cadetblue;'></i></a>" . 
                        "</td>".
                        "</tr>";
                        $count++;
                    }

                    $paginador = "<p>Resultados " . $data["pagi_info"] . "</p><p>" . $data["pagi_navegacion"] . "</p>";
                    echo json_encode( array(
                        "dataFilter" => $dataFilter,
                        "paginador"  => $paginador
                    ));

                } else {
                    echo $data;
                }
                

            }
        }

        # Método de acción para ejecutar vista de Cartera
        
        // public function reportes($email) {
        public function reportes() {
            $user = Session::getSession("User");

            // $email = $_GET['email'];

            if ($user != null) {

                if ($user["roles"]=="Admin") {
                    // echo $email;
                    // $this->view->render($this, "reportes", $email);
                    // $this->view->render($this, "reportes", $_GET['email']);
                    $this->view->render($this, "reportes", null);
                } else {
                    // $this->view->render($this,"clientes");
                    header("location:".URL."Clientes/clientes");
                }   

            }    
        }

        # Retorna información del clinete (Información de Cartera)
        # 04.05.2020 Dev. FMBM

        public function getReporteCliente() {
            $user = Session::getSession("User");

            if ($user != null) {
                if ($user["roles"]=="Admin") {
                    if (filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
                        $data = $this->model->getReporteCliente($_POST['email']);

                        if (is_array($data)) {
                            echo json_encode( array(
                                "array" => $data,
                                "data"  => 1
                            ));
                        } else {
                            echo json_encode( array(
                                "data" => 0
                            ));
                        }

                    } else {
                        echo json_encode( array(
                            "data" => 0
                        ));
                    }
                    
                }    
            }     
        }

        /**
         * Realiza abonos a deuda de cliente
         * 08.05.2020 Dev. FMBM
         */

        public function setPagos()
        {
            $user = Session::getSession("User");
            if ($user != null) {

                date_default_timezone_set('UTC');

                if ($user["roles"]=="Admin") {
                    $pago = (float) $_POST['pagos'];

                    if (is_float($pago) && $pago > 0) {
                        
                        $pago  = number_format($pago);

                        /*
                        // Obtener la información enviada desde el cliente (Frontend)
                        $array = json_decode($_POST["report"], true);
                        $array = $array['array'];
                        */

                        // Obteniendo los datos del servidor (Backend)
                        $array = Session::getSession("reportCliente");
                        $deuda = (float) str_replace("$", "", $array["Deuda"]);

                        if ($deuda == 0) {
                            echo "El cliente no tiene deuda";
                        } else {

                            if ($deuda < $pago) {
                                echo "El Pago supera el monto de la deuda";
                            } else {
                                $deuda = $deuda - $pago;

                                // Para insertar en la tabla registro_cliente
                                $arrayReport = array(
                                    "$".number_format($deuda),
                                    date("d-m-Y"),
                                    "$".$pago,
                                    date("d-m-Y"),
                                    $array["Ticket"],
                                    $array["IdClientes"]
                                );
                                
                                // Para inserción el la tabla ticket
                                $ticket = array(
                                    "Cliente" ,
                                    "$".number_format($deuda),
                                    date("d-m-Y"),
                                    "$".$pago,
                                    date("d-m-Y"),
                                    $array["Ticket"],
                                    $array["Email"]
                                );

                                // Se crea un objeto anónimo de la clase Tocket y se para como parámetro con el array $ticket
                                echo $this->model->setPagos($this->reportClientesClass($arrayReport),$this->ticketClass($ticket),$array["IdReportes"]);
                            }
                           
                        }
                        
                    } else {
                        echo "El dato ingresado no es correcto";
                    }
                    
                }
            }        
        }

        /**
        * Actualizar la información de un cliente
        * 28.06.2020 Dev. FMBM
        */

        public function editCliente()
        {
            $user = Session::getSession("User");
            if(null != $user){
                if ("Admin"== $user["roles"]) {
                    if (empty($_POST["nombre"])) {
                        echo "El campo nombre es obligatorios";
                    } else {
                        if (empty($_POST["apellido"])) {
                            echo "El campo apellido es obligatorios";
                        } else {
                            if (empty($_POST["nid"])) {
                                echo "El campo nid es obligatorios";
                            } else {
                                if (empty($_POST["telefono"])) {
                                    echo "El campo telefono es obligatorios";
                                } else {
                                    if (empty($_POST["email"])) {
                                        echo "El campo email es obligatorios";
                                    } else {
                                        if (empty($_POST["direccion"])) {
                                            echo "El campo direccion es obligatorios";
                                        } else {

                                            $array1 = array($_POST["nid"],$_POST["nombre"],$_POST["apellido"],$_POST["telefono"],$_POST["email"],$_POST["direccion"],$_POST["creditos"]);
                                            $data   = $this->model->editCliente($_POST["idCliente"],$this->clientesClass($array1));

                                            if ($data == 1) {
                                                echo "El email ".$_POST["email"]." ya esta registrado..   ";
                                            } else {

                                                if ($data == 0){

                                                    if(isset($_FILES['file'])){
                                                        $this->tipo    =  $_FILES['file']["type"];
                                                        $this->archivo =  $_FILES['file']["tmp_name"];
                                                    }

                                                    $this->image->cargar_imagen($this->tipo,$this->archivo,$_POST["email"],"clientes");
                                                    echo 0;
                                                }else{
                                                    echo $data;
                                                }

                                            }

                                        }
                                    }
                                }
                            }
                        }
                    }
                    
                } else {
                    echo "No tiene autorizacion";
                }
            }

        } 
        
        /**
         * Retorna la información de tickets o pagos de los clientes
         * 28.06.2020 - Dev. FMBM
         */

        public function getTickets(){

            $user = Session::getSession("User");
            if(null != $user ){

                $dataFilter = null;
                $data       = $this->model->getTickets($_POST["search"], $_POST["page"],$this->page); // Se le pasa al método lo parámetros y el paginador $page
                
                if (is_array($data)) {
                    
                    foreach ($data["results"] as $key => $value) {
                        $dataFilter .= "<tr>" .                    
                            "<td>".$value["Deuda"]."</td>".
                            "<td>".$value["FechaDeuda"]."</td>".
                            "<td>".$value["Pago"]."</td>".
                            "<td>".$value["FechaPago"]."</td>".
                            "<td>".$value["Ticket"]."</td>".
                            "</td>".
                        "</tr>";
                    }

                    $paginador ="<p>Resultados " .$data["pagi_info"]."</p><p>".$data["pagi_navegacion"]."</p> ";
                    
                    // Empaqueto los resultador y la info del paginador en un Json y lo retorno al Javascript
                    
                    echo json_encode( array(
                        "dataFilter" => $dataFilter,
                        "paginador"  => $paginador
                    ));

                } else {
                    return $data;
                }
                
            }
        }

        /**
         * Exporta información de los Tickets a Excel
         * 28.06.2020 Dev. FMBM
         */
        
        public function exportarExcel()
        {
            $user = Session::getSession("User");
            if(null != $user ){

                $archivo = null;
                $title = null;
                $data = null;

                if (1 == $_POST["valor"]) {

                    $title   = "Ticket";
                    $archivo = "TicketClientes.xls";
                    $data    = $this->model->getTickets($_POST["search"],$_POST["page"],$this->page);

                } else {

                    $title   = "Clientes";
                    $archivo = "Clientes.xls";
                    $data    = $this->model->getClientes($_POST["search"],$_POST["page"],$this->page);

                }
                
                if (is_array($data)) {
                    $this->export->exportarExcel($data["results"],$archivo,$title);
                }else{
                    return $data;
                }

            }
        }

    }

?>
