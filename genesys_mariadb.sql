-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         10.1.37-MariaDB - mariadb.org binary distribution
-- SO del servidor:              Win32
-- HeidiSQL Versión:             11.2.0.6213
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Volcando estructura de base de datos para genesys_db
CREATE DATABASE IF NOT EXISTS `genesys_db` /*!40100 DEFAULT CHARACTER SET latin1 COLLATE latin1_general_ci */;
USE `genesys_db`;

-- Volcando estructura para tabla genesys_db.clientes
CREATE TABLE IF NOT EXISTS `clientes` (
  `IdClientes` int(11) NOT NULL AUTO_INCREMENT,
  `NID` varchar(45) COLLATE latin1_general_ci DEFAULT NULL,
  `Nombre` varchar(45) COLLATE latin1_general_ci DEFAULT NULL,
  `Apellido` varchar(45) COLLATE latin1_general_ci DEFAULT NULL,
  `Email` varchar(45) COLLATE latin1_general_ci DEFAULT NULL,
  `Telefono` varchar(45) COLLATE latin1_general_ci DEFAULT NULL,
  `Direccion` varchar(45) COLLATE latin1_general_ci DEFAULT NULL,
  `Creditos` varchar(45) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`IdClientes`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci COMMENT='File with information of Clientes';

-- Volcando datos para la tabla genesys_db.clientes: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `clientes` DISABLE KEYS */;
INSERT INTO `clientes` (`IdClientes`, `NID`, `Nombre`, `Apellido`, `Email`, `Telefono`, `Direccion`, `Creditos`) VALUES
	(3, '10684587856', 'Alaín José', 'Jaramillo López', 'alain@hotmail.com', '3145067828', 'calle 64 no 14 23', '$2000.00'),
	(4, '724587856', 'Manuel Fernando', 'Llamas Hernández', 'mllamas@yahoo.com', '3145879526', 'calle 61 no 35 48 Castellana', '$2000.00');
/*!40000 ALTER TABLE `clientes` ENABLE KEYS */;

-- Volcando estructura para tabla genesys_db.creditos
CREATE TABLE IF NOT EXISTS `creditos` (
  `IdCreditos` int(11) NOT NULL AUTO_INCREMENT,
  `Creditos` varchar(45) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`IdCreditos`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci COMMENT='File configuration of limits of Creditos';

-- Volcando datos para la tabla genesys_db.creditos: ~5 rows (aproximadamente)
/*!40000 ALTER TABLE `creditos` DISABLE KEYS */;
INSERT INTO `creditos` (`IdCreditos`, `Creditos`) VALUES
	(1, '$0.00'),
	(2, '$500.00'),
	(3, '$1000.00'),
	(4, '$1500.00'),
	(5, '$2000.00');
/*!40000 ALTER TABLE `creditos` ENABLE KEYS */;

-- Volcando estructura para tabla genesys_db.proveedores
CREATE TABLE IF NOT EXISTS `proveedores` (
  `IdProveedor` int(11) NOT NULL AUTO_INCREMENT,
  `Proveedor` varchar(45) COLLATE latin1_general_ci DEFAULT NULL,
  `Telefono` varchar(45) COLLATE latin1_general_ci DEFAULT NULL,
  `Email` varchar(45) COLLATE latin1_general_ci DEFAULT NULL,
  `Direccion` varchar(45) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`IdProveedor`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci COMMENT='File with information of Proveedores';

-- Volcando datos para la tabla genesys_db.proveedores: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `proveedores` DISABLE KEYS */;
INSERT INTO `proveedores` (`IdProveedor`, `Proveedor`, `Telefono`, `Email`, `Direccion`) VALUES
	(2, 'Proveerora del Sureste Antioqueño', '319 258 56 47', 'laproveedora@ggg.com', 'Vía a Planetarica km 11');
/*!40000 ALTER TABLE `proveedores` ENABLE KEYS */;

-- Volcando estructura para tabla genesys_db.reportes_clientes
CREATE TABLE IF NOT EXISTS `reportes_clientes` (
  `IdReportes` int(11) NOT NULL AUTO_INCREMENT,
  `Deuda` varchar(45) COLLATE latin1_general_ci DEFAULT NULL,
  `FechaDeuda` varchar(45) COLLATE latin1_general_ci DEFAULT NULL,
  `Pago` varchar(45) COLLATE latin1_general_ci DEFAULT NULL,
  `FechaPago` varchar(45) COLLATE latin1_general_ci DEFAULT NULL,
  `Ticket` varchar(45) COLLATE latin1_general_ci DEFAULT NULL,
  `IdClientes` int(11) NOT NULL,
  PRIMARY KEY (`IdReportes`),
  KEY `fk_reportes_clientes_clientes_idx` (`IdClientes`),
  CONSTRAINT `fk_reportes_clientes_clientes` FOREIGN KEY (`IdClientes`) REFERENCES `clientes` (`IdClientes`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci COMMENT='File with credit status of Clientes';

-- Volcando datos para la tabla genesys_db.reportes_clientes: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `reportes_clientes` DISABLE KEYS */;
INSERT INTO `reportes_clientes` (`IdReportes`, `Deuda`, `FechaDeuda`, `Pago`, `FechaPago`, `Ticket`, `IdClientes`) VALUES
	(1, '$150', '24-06-2020', '$40', '24-06-2020', '0123', 3),
	(2, '$0.00', '--/--/--', '$0.00', '--/--/--', '000000', 4);
/*!40000 ALTER TABLE `reportes_clientes` ENABLE KEYS */;

-- Volcando estructura para tabla genesys_db.reportes_proveedores
CREATE TABLE IF NOT EXISTS `reportes_proveedores` (
  `IdReportes` int(11) NOT NULL AUTO_INCREMENT,
  `Deuda` varchar(45) COLLATE latin1_general_ci DEFAULT NULL,
  `FechaDeuda` varchar(45) COLLATE latin1_general_ci DEFAULT NULL,
  `Pago` varchar(45) COLLATE latin1_general_ci DEFAULT NULL,
  `FechaPago` varchar(45) COLLATE latin1_general_ci DEFAULT NULL,
  `Ticket` varchar(45) COLLATE latin1_general_ci DEFAULT NULL,
  `IdProveedor` int(11) NOT NULL,
  PRIMARY KEY (`IdReportes`),
  KEY `fk_reportes_idproveedor` (`IdProveedor`),
  CONSTRAINT `fk_reportes_idproveedor` FOREIGN KEY (`IdProveedor`) REFERENCES `proveedores` (`IdProveedor`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci COMMENT='File with credit status of Proveedores';

-- Volcando datos para la tabla genesys_db.reportes_proveedores: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `reportes_proveedores` DISABLE KEYS */;
INSERT INTO `reportes_proveedores` (`IdReportes`, `Deuda`, `FechaDeuda`, `Pago`, `FechaPago`, `Ticket`, `IdProveedor`) VALUES
	(2, '$138', '13-08-2020', '$11', '13-08-2020', '0000000002', 2);
/*!40000 ALTER TABLE `reportes_proveedores` ENABLE KEYS */;

-- Volcando estructura para tabla genesys_db.roles
CREATE TABLE IF NOT EXISTS `roles` (
  `idRole` int(11) NOT NULL AUTO_INCREMENT,
  `role` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`idRole`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci COMMENT='File configuration of roles for users';

-- Volcando datos para la tabla genesys_db.roles: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` (`idRole`, `role`) VALUES
	(1, 'Admin'),
	(2, 'User');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;

-- Volcando estructura para tabla genesys_db.ticket
CREATE TABLE IF NOT EXISTS `ticket` (
  `IdTicket` int(11) NOT NULL AUTO_INCREMENT,
  `Propietario` varchar(45) COLLATE latin1_general_ci DEFAULT NULL,
  `Deuda` varchar(45) COLLATE latin1_general_ci DEFAULT NULL,
  `FechaDeuda` varchar(45) COLLATE latin1_general_ci DEFAULT NULL,
  `Pago` varchar(45) COLLATE latin1_general_ci DEFAULT NULL,
  `FechaPago` varchar(45) COLLATE latin1_general_ci DEFAULT NULL,
  `Ticket` varchar(45) COLLATE latin1_general_ci DEFAULT NULL,
  `Email` varchar(45) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`IdTicket`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci COMMENT='File con el registro de pago de Clientes y Proveedores';

-- Volcando datos para la tabla genesys_db.ticket: ~3 rows (aproximadamente)
/*!40000 ALTER TABLE `ticket` DISABLE KEYS */;
INSERT INTO `ticket` (`IdTicket`, `Propietario`, `Deuda`, `FechaDeuda`, `Pago`, `FechaPago`, `Ticket`, `Email`) VALUES
	(1, 'Cliente', '$150', '24-06-2020', '$40', '24-06-2020', '0123', 'alain@hotmail.com'),
	(2, 'Proveedor', '$149,940', '13-08-2020', '$60,000', '13-08-2020', '0000000001', 'laproveedora@ggg.com'),
	(3, 'Proveedor', '$138', '13-08-2020', '$11', '13-08-2020', '0000000002', 'laproveedora@ggg.com');
/*!40000 ALTER TABLE `ticket` ENABLE KEYS */;

-- Volcando estructura para tabla genesys_db.usuarios
CREATE TABLE IF NOT EXISTS `usuarios` (
  `idUsuario` int(11) NOT NULL AUTO_INCREMENT,
  `nid` varchar(50) COLLATE latin1_general_ci NOT NULL DEFAULT '0',
  `pnombre` varchar(50) COLLATE latin1_general_ci NOT NULL DEFAULT '0',
  `snombre` varchar(50) COLLATE latin1_general_ci NOT NULL DEFAULT '0',
  `apellidos` varchar(50) COLLATE latin1_general_ci NOT NULL DEFAULT '0',
  `email` varchar(50) COLLATE latin1_general_ci NOT NULL DEFAULT '0',
  `telefono` varchar(50) COLLATE latin1_general_ci NOT NULL DEFAULT '0',
  `password` char(255) COLLATE latin1_general_ci NOT NULL DEFAULT '0',
  `usuario` varchar(50) COLLATE latin1_general_ci NOT NULL DEFAULT '0',
  `roles` varchar(50) COLLATE latin1_general_ci NOT NULL DEFAULT '0',
  `imagen` varchar(50) COLLATE latin1_general_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`idUsuario`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci COMMENT='File configuration of system Users';

-- Volcando datos para la tabla genesys_db.usuarios: ~4 rows (aproximadamente)
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` (`idUsuario`, `nid`, `pnombre`, `snombre`, `apellidos`, `email`, `telefono`, `password`, `usuario`, `roles`, `imagen`) VALUES
	(1, '78712094', 'Francisco', 'Miguel', 'Berrocal Machado', 'fberrocal73@hotmail.com', '0', '$2y$10$IzpUU2R3tZGrgKR.moelAuiRKQ.KQ/XVgiC.j9sv4QIjkMsBs811i', 'fberrocal', 'Admin', 'default.png'),
	(7, '10684568572', 'Joel', 'Enrique', 'Villa Villa', 'joel@yahoo.com', '3185678546', '$2y$10$SJm3G9JV7pGUZELOMtv2G.teFP5Wo.Bbk2NkmmYRV8qXuzIUcaFMi', 'jvillal', 'User', 'joel@yahoo.com.png'),
	(9, '508785623', 'Edgardo', 'José', 'Bolivar Valderrama', 'edgardo_bolivar@gmail.com', '3185687845', '$2y$10$sWCw7UPcMD1IyYZvFnpa7OMAbN8SOEfUcpKsH3SKr.t3qwMYTt3zO', 'ebolivar', 'User', 'edgardo_bolivar@gmail.com.png'),
	(13, '10687584523', 'Adriana', 'Patricia', 'Berrocal Gómez', 'aberrocal97@gmail.com', '3175689643', '$2y$10$RSfWrJ1.dDIDy5jvI4sl3uv.VTYja/Iez4oJGPbpzc7RE0JzFwaUe', 'aberrocalg', 'User', 'aberrocal97@gmail.com.png'),
	(14, '106845285457', 'Adriana', 'Patricia', 'Berrocal Gómez', 'f@admin.com', '3174582567', '$2y$10$WmCvoH7AY7GiKy83AwfuiObPPQyJPSWiqZR9zrg6tV.E4GWe1vrea', 'apgomez', 'User', 'f@admin.com.png');
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
